public class Board{ 
  /*fields*/
  private Square[][] tictactoeBoard; 
  
  /*Constructor that makes new tictactoe board*/
  public Board(){ 
    /* this.tictactoeBoard = new Square[][] {{Square.BLANK, Square.BLANK, Square.BLANK},
     {Square.BLANK, Square.BLANK, Square.BLANK}, 
     {Square.BLANK, Square.BLANK, Square.BLANK}};*/
    this.tictactoeBoard = new Square[3][3];
    this.tictactoeBoard[0][0] = Square.BLANK;
    this.tictactoeBoard[0][1] = Square.BLANK;
    this.tictactoeBoard[0][2] = Square.BLANK;
    this.tictactoeBoard[1][0] = Square.BLANK;
    this.tictactoeBoard[1][1] = Square.BLANK;
    this.tictactoeBoard[1][2] = Square.BLANK;
    this.tictactoeBoard[2][0] = Square.BLANK;
    this.tictactoeBoard[2][1] = Square.BLANK;
    this.tictactoeBoard[2][2] = Square.BLANK; 
  }
  /*Prints the boards current game status*/
  public String toString(){ 
    String placement = " ";
    /*for(int r=0; r<tictactoeBoard.length; r++){ 
     for(int c=r; c<tictactoeBoard.length; c++){
     placement += tictactoeBoard[r][c] + " ";
     return placement;
     }
     System.out.println();
     }*/
    for(Square[] tempBoard : tictactoeBoard){  
      for(Square tempValue : tempBoard){
        placement = "[" + tempValue + "]"; 
        System.out.print(placement);
      } 
      System.out.println();
    }
    placement = " ";
    return placement;
  }
  
  /*Action method that validates player move and executes it if valid*/
  public boolean placeToken(int row, int col, Square playerToken){ 
    boolean v = false;
    if( row > 2 || col > 2){
      return v;
    }else if(this.tictactoeBoard[row][col].equals(Square.BLANK)){
      this.tictactoeBoard[row][col] = playerToken;
      v = true;
      return v;
    }else{
    } 
    return v;
  }
  
  /*checks if the board is full*/
  public boolean checkIfFull(){
    boolean v = false;
    int count = 0;
    for(Square[] tempBoard : tictactoeBoard){
      for(Square tempValue : tempBoard){
        if(tempValue.equals(Square.BLANK)){ 
          return v;
        }else{ 
          count++;
        }
      }
      if(count  == 9){
        v = true;
      }    
    }
    return v;
  }
  /*checks if the player wins by a horizontal line*/
  private boolean checkIfWinningHorizontal(Square playerToken){ 
    boolean won = false;
    for(int r=0; r<this.tictactoeBoard.length; r++){ 
      int token = 0;
      for(int c=0; c<this.tictactoeBoard.length; c++){
        if(tictactoeBoard[r][c] == playerToken){ 
          token++;
        } 
        if(token == 3){ 
          won = true;
          return won;
        } 
      }
    }
    return won;
  }
  
  /*checks if the player wins by a vertical line*/
  private boolean checkIfWinningVertical(Square playerToken){ 
    boolean won = false;
    for(int c=0; c<this.tictactoeBoard.length; c++){ 
      int token = 0;
      for(int r=0; r<this.tictactoeBoard.length; r++){
        if(tictactoeBoard[r][c] == playerToken){ 
          token++;
        } 
        if(token == 3){ 
          won = true;
          return won;
        } 
      }
    }
    return won;   
  }
  
  /*checks if the player wins diagonally*/ 
  private boolean checkIfWinningDiagonally(Square playerToken){ 
    boolean won = false;
    for(int r=0;  r<this.tictactoeBoard.length; r++){
      int token = 0;
      if(tictactoeBoard[r][r] == playerToken){ 
        token++;
      } 
      if(token == 3){ 
        won = true;
        return won;
      } 
    }
    int token = 0;
    for(int r=this.tictactoeBoard.length-1;  r>((this.tictactoeBoard.length-this.tictactoeBoard.length)-1); r--){   
      for(int c=((this.tictactoeBoard.length-1)-r); c<(this.tictactoeBoard.length-r); c++){
        if(tictactoeBoard[r][c] == playerToken){ 
          token++;
        } 
        if(token == 3){ 
          won = true;
          return won;
        } 
      } 
    }
    return won;
  } 
  
  /*checks if the player wins horizontally or vertically*/
  public boolean checkIfWinning(Square playerToken){ 
    boolean won = false;
    if(checkIfWinningHorizontal(playerToken) == true || checkIfWinningVertical(playerToken) == true || checkIfWinningDiagonally(playerToken) == true){
      won = true;
      return won;
    } 
    return won;
  }   
}