import java.util.Scanner;
public class TicTacToeGame{ 
  public static void main(String[] args){ 
    
    /*Setting up Game*/
    boolean gameOver = false;
    int player = 1;
    Square playerToken = Square.X;
    System.out.println("Welcome to TicTacToe Java Edition!"); 
    System.out.println("Player " + player + "'s token: " + playerToken);
    System.out.println("Player " + (player+1) + "'s token: " + Square.O);
    Board gameBoard = new Board(); 
    
    /*Game loop*/
    while (gameOver != true){
      Scanner reader = new Scanner(System.in);
      System.out.println();
      System.out.println(gameBoard);
      
      /*player's token*/
      if(player == 1){
        playerToken = Square.X;
        System.out.println("Player " + player + ": it's your turn where do you want to place your token?");
        System.out.println("--------------------------->");
        
        /*Player plays choses and places token*/
        int r = reader.nextInt() - 1;
        int c = reader.nextInt() - 1;
        boolean v = gameBoard.placeToken(r, c, playerToken);
        if(v != true){
          while(v != true) {
            System.out.print("Please reenter a row:");
            r = reader.nextInt() - 1;
            System.out.print("Please reenter a column:"); 
            c = reader.nextInt() - 1;
            v = gameBoard.placeToken(r, c, playerToken);
          } 
        } 
      }else if(player != 1){
        playerToken = Square.O;
        System.out.println("Player " + player + ": it's your turn where do you want to place your token?");
        System.out.println("--------------------------->");
        
        /*Player plays choses and places token*/
        int r = reader.nextInt() - 1;
        int c = reader.nextInt() - 1;
        boolean v = gameBoard.placeToken(r, c, playerToken);
        if( v != true){
          while(v != true) {
            System.out.print("Please reenter a row:");
            r = reader.nextInt() - 1;
            System.out.print("Please reenter a column:"); 
            c = reader.nextInt() - 1;
            v = gameBoard.placeToken(r, c, playerToken);
          } 
        }
      }  
      boolean full = gameBoard.checkIfFull();
      if( full != false){ 
        System.out.println();
        System.out.println(gameBoard);
        System.out.println("It's a tie!"); 
        gameOver = true;
      }
      boolean winner = gameBoard.checkIfWinning(playerToken);
      if(winner != false){ 
        System.out.println();
        System.out.println(gameBoard);
        System.out.println("Player " + player + " is the winner!"); 
        gameOver = true;
      }else{ 
        player++; 
        if (player > 2){
          player = 1;
        } 
      }
    }
  }
}